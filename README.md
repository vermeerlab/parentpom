Parent Pom
===

Project で使用する共通pom

## Description

各プロジェクトのpomに以下を追記します.

```xml
<parent>
    <groupId>org.vermeerlab</groupId>
    <artifactId>parentpom</artifactId>
    <version>0.1.0</version>
    <relativePath>../</relativePath>
</parent>

```

## Usage

GitHubのMavenリポジトリにアップロードをしたい時は、子プロジェクト側で `mvn deploy` を実行します.

```xml
<plugins>
    <plugin>
        <groupId>org.apache.maven.plugins</groupId>
        <artifactId>maven-deploy-plugin</artifactId>
    </plugin>
    <plugin>
        <groupId>com.github.github</groupId>
        <artifactId>site-maven-plugin</artifactId>
    </plugin>
</plugins>
```

### 事前準備

#### GitHub

* GitHub上にリポジトリ（例：`maven`）を作成
* Branchを作成（例：`mvn-repo` ）
* GitPagesを作成
* 認証キーの取得（`public_repo`と`user:email`をチェック）

#### ローカル環境

* settings.xmlの編集

```xml
    <servers>
        <server>
            <id>github</id>
            <password>（取得した認証キー）</password>
        </server>
    </servers>
```


### アップロード先を変更したい場合

用途に応じて、パラメータを子プロジェクト側で指定します.

```xml

<properties>
    <github.global.server>github</github.global.server>
    <git.repositoryOwner>（ユーザー名 or Organization名）</git.repositoryOwner>
    <git.repositoryName>（リポジトリ名）</git.repositoryName>
</properties>

```

## Licence

Licensed under the [Apache License, Version 2.0](http://www.apache.org/licenses/LICENSE-2.0)

## Author

[_vermeer_](https://twitter.com/_vermeer_)